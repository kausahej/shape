/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shapes;

/**
 *
 * @author sahej
 */
public abstract class Shape
{  
public abstract double getArea();
public abstract double getPerimeter();

 }

class Circle extends Shape
{
private double radius;

public Circle(int raduis)
{
this.radius = radius;
}

@Override
public double getArea()
{
return 3.14*radius*radius;

}

@Override
public double getPerimeter()
{
return 2*3.14*radius;

}


class Square extends Shape
{
private double side;

public Square( double side)
{
this.side = side;
}

@Override
public double getArea()
{
return Math.pow(side, 2);

}

@Override
public double getPerimeter()
{
return 4*side;




}
}
}

